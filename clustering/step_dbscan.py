from __future__ import division, print_function
from six.moves import range
from cctbx.array_family import flex
from libtbx.development.timers import SlimProfiler as Profiler
from matplotlib import pyplot as plt
import numpy as np
from math import sqrt

"""
Initial implementation of clustering by sklearn's DBSCAN
"""

from uc_metrics.clustering.step1 import app_set_up

class feature_vectors:
  """lightweight container for serial crystallography experimental unit cells.
  Largest potential performance sinks are the instantiation of crystal.symmetry, and calculation of niggli cell.
  Therefore, use experimental cell directly from the indexed Bravais cell; without using crystal.symmetry
  Example:  Lysozyme, choose (a,c) to be the feature vector, and filter on P4/mmm symmetry.
  Here, we do not have the extra baggage of carrying around a full unit cell, only the a and c cell lengths.
  """
  def __init__(self, fv, features, sample_name=""):
    self.fv_ = fv
    self.features_ = features
    self.sample_name = sample_name

  @classmethod
  def reject_outliers(cls, data, iqr_ratio = 1.5):
    """
    Copypasta from xfel/ui/components/xfel_gui_plotter.py
    @param data is a 1d array
    """
    data = flex.double(data)
    eps = 1e-6
    outliers = flex.bool(len(data), False)
    if iqr_ratio is None:
      return outliers
    from scitbx.math import five_number_summary
    min_x, q1_x, med_x, q3_x, max_x = five_number_summary(data)
    iqr_x = q3_x - q1_x
    cut_x = iqr_ratio * iqr_x
    outliers.set_selected(data > q3_x + cut_x + eps, True)
    outliers.set_selected(data < q1_x - cut_x - eps, True)
    return outliers

  @classmethod
  def from_tdata(cls, params, store_cell=False):
    X_train = flex.double()
    assert params.input.space_group is not None
    if store_cell:
      from cctbx.uctbx import unit_cell as cctbx_uc
      UC_train = []
    assert params.input.feature_vector is not None
    feature_vector_list = params.input.feature_vector.strip("(").strip(")").split(",")
    interpret_fv = [
      dict(a=0, b=1, c=2, alpha=3, beta=4, gamma=5)[item] for item in feature_vector_list
    ]

    if params.plot.legend:
      legend = params.plot.legend
    elif params.file_name:
      import os
      legend = os.path.splitext(os.path.basename(params.file_name))[0]
    else:
      legend = ""

    if params.file_name:
      iterable = open(params.file_name, "r")
    else:
      iterable = params.input.iterable
    iterable = list(iterable) # we need to use it twice

    # now identify outliers
    uc_tuples = []
    for line in iterable:
      tokens = line.strip().split()
      unit_cell = tuple(float(x) for x in tokens[0:6])
      space_group_symbol = tokens[6]
      if space_group_symbol != params.input.space_group: continue
      uc_tuples.append(unit_cell)
    if not uc_tuples:
      raise KeyError('No cells match space group ' + params.input.space_group)
    accepted = flex.bool(len(uc_tuples), True)
    iqr_ratio = params.filter.iqr_ratio
    if iqr_ratio is not None:
      for uc_param in zip(*uc_tuples):
        outliers = cls.reject_outliers(uc_param, iqr_ratio=iqr_ratio)
        accepted &= ~outliers

    for i_line, line in enumerate(iterable):
      if not accepted[i_line]: continue
      P = Profiler("0A1. Read data")
      tokens = line.strip().split()
      P = Profiler("0A2. Read data")
      unit_cell = tuple(float(x) for x in tokens[0:6])
      P = Profiler("0A3. Read data")
      space_group_symbol = tokens[6]
      if space_group_symbol != params.input.space_group: continue
      if store_cell:
        UC_train.append(cctbx_uc(unit_cell))
      for idx_position in interpret_fv:
        X_train.append(unit_cell[idx_position])

    Ncells = len(X_train)//len(interpret_fv)
    X_train.reshape(flex.grid(Ncells,len(interpret_fv)))
    X_train = X_train.as_numpy_array()
    print("There are %d cells"%(Ncells))
    CLS = cls( fv = X_train, features = feature_vector_list ,sample_name=legend)
    if store_cell: CLS.UC_train = UC_train
    return CLS

  @classmethod
  def from_tdata_store_cell(cls, params):
    return cls.from_tdata(params, store_cell=True)

def run_detail(save_plot):
    params, options = app_set_up()

    FV = feature_vectors.from_tdata(params)
    X_train = FV.fv_

    coord_x = X_train[:, 0]; coord_y = X_train[:, 1]
    if params.show_plot or save_plot:
      import matplotlib
      if not params.show_plot:
        # http://matplotlib.org/faq/howto_faq.html#generate-images-without-having-a-window-appear
        matplotlib.use('Agg') # use a non-interactive backend
      plt.plot(coord_x,coord_y,"k.", markersize=3.)
      plt.xlabel("%s"%(FV.features_[0]))
      plt.ylabel("%s"%(FV.features_[1]))
      plt.axes().set_aspect("equal")
      if save_plot:
        plt.savefig(plot_name,
                    size_inches=(10,10),
                    dpi=300,
                    bbox_inches='tight')
      if params.show_plot:
        plt.show()

    P = Profiler("sklearn")

    from sklearn.cluster import DBSCAN
    GM = DBSCAN(eps=params.cluster.dbscan.eps)
    Lab = GM.fit_predict(X_train)

    from uc_metrics.examples.util import get_population_permutation
    pop = get_population_permutation(Lab) # largest-to-smallest components, Lab indirection
    pop.basic_covariance(feature_vectors = FV) # report out clusters

    if params.show_plot or save_plot:

      P = Profiler("plot")
      appcolors = ['b', 'r', '#ff7f0e', '#2ca02c',
              '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
              '#bcbd22', '#17becf', 'k',
              'k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k',]

      colors = [appcolors[ pop.reverse_lookup[Lab[i]] ] for i in range(len(Lab))]

      plt.scatter(X_train[:, 0], X_train[:, 1], .8, color=colors)
      for jitem in range(len(pop.fit_components)):
        COV = pop.fit_components[jitem]
        plt.scatter( [COV.location_[0]], [COV.location_[1]], 3.0, color="yellow", label="mean")

        import matplotlib as mpl
        # Show contours of the distance functions
        xx, yy = np.meshgrid(
          np.linspace(COV.location_[0]-5*sqrt(COV.covariance_[0,0]), COV.location_[0]+5*sqrt(COV.covariance_[0,0]), 100),
          np.linspace(COV.location_[1]-5*sqrt(COV.covariance_[1,1]), COV.location_[1]+5*sqrt(COV.covariance_[1,1]), 100),
        )
        zz = np.c_[xx.ravel(), yy.ravel()]

        mahal_emp_cov = COV.mahalanobis(zz)
        mahal_emp_cov = mahal_emp_cov.reshape(xx.shape)
        emp_cov_contour = plt.contour(xx, yy, np.sqrt(mahal_emp_cov),
                                  levels=[1.,2.,3.,4.],
                                  cmap=plt.cm.PuBu_r,
                                  norm = mpl.colors.Normalize(vmin=0.0, vmax=5.0), #sensible color range
                                  linestyles='dashed')
        plt.annotate("%d"%(jitem),
                     xy = (COV.location_[0]-4.5*sqrt(COV.covariance_[0,0]),
                           COV.location_[1]-4.5*sqrt(COV.covariance_[1,1])))

      plt.title('Dbscan clustering')
      plt.xlabel("%s"%(FV.features_[0]))
      plt.ylabel("%s"%(FV.features_[1]))
      plt.axis('tight')
      plt.show()


if __name__=="__main__":

  run_detail(save_plot=False)
""" Benchmark, Lysozyme lattices, Linux AMD Athlon, single process
          Lattices              1341        13004           20000      137347
          # cores                  1            1               1           1
     dbscan eps parameter       0.06         0.05            0.05        0.02
   0A1. Tokenize data: wall     0.01s;       0.12s;          0.19s;      1.37s
 0A2. Parse unit cell: wall     0.02s;       0.16s;          0.26s;      1.90s
 0A3. Feature vectors: wall     0.01s;       0.14s;          0.21s;      1.49s
       sklearn dbscan: wall     0.50s;       1.55s;          2.69s;     22.85s
TOTAL                : wall     0.55s;       1.97s;          3.35s;     27.38s
"""
