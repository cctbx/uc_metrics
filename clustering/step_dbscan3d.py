from __future__ import division, print_function
"""
3D (orthorhombic) implementation of clustering by sklearn's DBSCAN
"""

from uc_metrics.clustering.step1 import app_set_up
from uc_metrics.clustering.step_dbscan import feature_vectors
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec
from libtbx.development.timers import SlimProfiler as Profiler
import numpy as np
from math import sqrt, copysign
from scitbx.array_family import flex

class dbscan_plot_manager:
  def __init__(self, params, save_plot=False):
    self.params = params
    self.save_plot = save_plot
    # determine crystal_system here so we can make some decisions:
    from cctbx.sgtbx import space_group_info
    SG = space_group_info(params.input.space_group)
    CS = SG.group().crystal_system()
    self.is_monoclinic = CS == "Monoclinic"
    self.is_triclinic = CS == "Triclinic"

    if self.is_monoclinic or self.is_triclinic: self.FV = feature_vectors.from_tdata_store_cell(params)
    else: self.FV = feature_vectors.from_tdata(params)
    self.FV.output_info = "eps=%.3f"%(params.cluster.dbscan.eps) # important to preserve dbscan parameter
    P = Profiler("sklearn")

    # configure the clustering
    from sklearn.cluster import DBSCAN
    from sklearn.mixture import GaussianMixture
    if params.cluster.algorithm == 'dbscan':
      clustering = DBSCAN
      cluster_kwargs = {
          'eps': params.cluster.dbscan.eps,
          'min_samples': params.cluster.dbscan.min_samples
          }
    elif params.cluster.algorithm == 'GaussianMixture':
      clustering = GaussianMixture
      cluster_kwargs = {
          'n_components': params.cluster.gaussian_mixture.n_components,
          'covariance_type': 'full',
          'init_params': 'random',
          }
      means_init = params.cluster.gaussian_mixture.means_init
      if means_init:
        for mean in means_init:
          assert len(mean) == len(self.FV.features_)
        assert len(means_init) == cluster_kwargs['n_components']
        cluster_kwargs['means_init'] = means_init
    else:
      cluster_kwargs = {}
    self.GM = clustering(**cluster_kwargs)
    self.Lab = self.GM.fit_predict(self.FV.fv_)

    from uc_metrics.clustering.util import get_population_permutation
    self.pop = get_population_permutation(self.Lab) # largest-to-smallest components, Lab indirection
    self.pop.basic_covariance(feature_vectors = self.FV) # report out clusters
    self.legend = self.pop.basic_covariance_compact_report(feature_vectors = self.FV, params=params).getvalue()
    print(self.legend)
    if params.write_covariance is not None:
      self.covariance_model_as_pickle()
    if self.params.plot.outliers:
      self.appcolors = ['k']*len(self.pop.reverse_lookup) # bugfix, pad with black
    else:
      self.appcolors = ['w']*len(self.pop.reverse_lookup) # pad with white to erase outliers
    ref_colors = ['b', 'r', '#ff7f0e', '#2ca02c', '#9467bd',
                  '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
    for cidx in range(min(len(ref_colors), len(self.pop.fit_components))):
      self.appcolors[cidx] = ref_colors[cidx]
    self.colors = [self.appcolors[ self.pop.reverse_lookup[self.Lab[i]] ] for i in range(len(self.Lab))]

  def covariance_model_as_dict(self):
    return dict(
      populations=self.pop,features=self.FV.features_,info=self.FV.output_info,sample=self.FV.sample_name )

  def covariance_model_as_pickle(self, outfile=None):
    if outfile is None:
      outfile = "covariance_%s.pickle"%(self.FV.sample_name.strip())
    import pickle
    # or change this code to accept phil output file root:
    with open(outfile,'wb') as FF:
      pickle.dump(  self.covariance_model_as_dict(), FF  )

  def plot_raw_uc(self):
    X_train = self.FV.fv_
    coord_x = X_train[:, 0]; coord_y = X_train[:, 1]
    if self.params.show_plot or save_plot:
      import matplotlib
      if not self.params.show_plot:
        # http://matplotlib.org/faq/howto_faq.html#generate-images-without-having-a-window-appear
        matplotlib.use('Agg') # use a non-interactive backend
      plt.plot(coord_x,coord_y,"k.", markersize=3.)
      plt.xlabel("%s"%(self.FV.features_[0]))
      plt.ylabel("%s"%(self.FV.features_[1]))
      plt.axes().set_aspect("equal")
      if self.save_plot:
        plt.savefig(plot_name,
                    size_inches=(10,10),
                    dpi=300,
                    bbox_inches='tight')
      if self.params.show_plot:
        plt.show()

  def plot_2D_features(self, ax=(1,2), plt=plt):
    if self.params.show_plot or self.save_plot:

      P = Profiler("plot")
      X_train = self.FV.fv_
      flex_X = flex.double(np.array(X_train[:, ax[0] ]))
      flex_Y = flex.double(np.array(X_train[:, ax[1] ]))
      display = flex.bool()
      dispcol = []
      for idx in range(len(self.colors)):
        if self.colors[idx]=='w':
          display.append(False)
        else:
          display.append(True)
          dispcol.append(self.colors[idx])
      plt.scatter(flex_X.select(display), flex_Y.select(display), .8, color=dispcol)
      for jitem in range(len(self.pop.fit_components)):
        COV = self.pop.fit_components[jitem]
        from sklearn.covariance import EmpiricalCovariance as EC
        COV_slice = EC()
        COV_slice.location_ = np.array([ COV.location_[ax[0]], COV.location_[ax[1]] ])
        COV_slice.covariance_ = np.array([ COV.covariance_[ ax[0],ax[0] ], COV.covariance_[ ax[0],ax[1] ],
                                           COV.covariance_[ ax[1],ax[0] ], COV.covariance_[ ax[1],ax[1] ] ])
        COV_slice.covariance_ = COV_slice.covariance_.reshape((2,2))
        COV_slice.precision_ = np.array([ COV.precision_[ ax[0],ax[0] ], COV.precision_[ ax[0],ax[1] ],
                                           COV.precision_[ ax[1],ax[0] ], COV.precision_[ ax[1],ax[1] ] ])
        COV_slice.precision_ = COV_slice.precision_.reshape((2,2))

        plt.scatter( [COV_slice.location_[0]], [COV_slice.location_[1]], 3.0, color="yellow", label="mean")

        if self.params.plot.ellipsoids:
          import matplotlib as mpl
          # Show contours of the distance functions
          xx, yy = np.meshgrid(
          np.linspace(COV_slice.location_[0]-5*sqrt(COV_slice.covariance_[0,0]), COV_slice.location_[0]+5*sqrt(COV_slice.covariance_[0,0]), 100),
          np.linspace(COV_slice.location_[1]-5*sqrt(COV_slice.covariance_[1,1]), COV_slice.location_[1]+5*sqrt(COV_slice.covariance_[1,1]), 100),
        )
          zz = np.c_[xx.ravel(), yy.ravel()]

          mahal_emp_cov = COV_slice.mahalanobis(zz)
          mahal_emp_cov = mahal_emp_cov.reshape(xx.shape)
          emp_cov_contour = plt.contour(xx, yy, np.sqrt(mahal_emp_cov),
                                  levels=[1.,2.,3.,4.],
                                  cmap=mpl.pyplot.cm.PuBu_r,
                                  norm = mpl.colors.Normalize(vmin=0.0, vmax=5.0), #sensible color range
                                  linestyles='dashed')
        plt.annotate("%d"%(jitem),
                     xy = (COV_slice.location_[0]-4.0*sqrt(COV_slice.covariance_[0,0]),
                           COV_slice.location_[1]-4.0*sqrt(COV_slice.covariance_[1,1])),
                     color=self.appcolors[jitem % len(self.appcolors)])

      plt.set_xlabel("$%s-$axis (Å)"%(self.FV.features_[ax[0]]))
      plt.set_ylabel("$%s-$axis (Å)"%(self.FV.features_[ax[1]]))
      if self.params.plot.limits.x is not None and self.params.plot.limits.y is not None:
        plt.set_xlim(self.params.plot.limits.x)
        plt.set_ylim(self.params.plot.limits.y)
      del P

  def qq(self, plt):
    import scipy.stats as stats
    if self.params.show_plot or self.save_plot:
      plt.set_title("Probability plot");
      plt.set_xlabel("Theoretical quantile"); plt.set_ylabel("Sample quantile")
      X_train = self.FV.fv_ # the actual data in terms of cell a,b,c as Nx3 numpy array
      labels = flex.size_t(self.pop.labels) # assignment of N cells into cluster number or -1 for no cluster
      def mul(item,cov):
        diff = item - cov.location_
        return diff[0]*diff[1]*diff[2] # ersatz sign for a three-parameter feature
      for jitem in range(len(self.pop.fit_components)-1,-1,-1):
        COV = self.pop.fit_components[jitem] # covariance matrix of component jitem
        isel = (labels==jitem).iselection() # addresses of those cells in cluster jitem
        filtered_train = X_train[isel] # just the M cells in the jitem component
        mahal = np.sqrt(COV.mahalanobis(filtered_train)) # the unsquared Mahalanobis distances
        # coax the distances to be both positive and negative
        signs = [copysign(mahal[idx],mul(filtered_train[idx],cov=COV)) for idx in range(len(mahal))]
        (osm,osr),(slope,intercept,r) = stats.probplot(signs, dist="norm", plot=None)
        plt.plot(osm,osr,color=self.appcolors[jitem])

  def plot_3D_features(self, fig=None, embedded=False):
      if not fig:
        fig = plt.figure(figsize=[12.8,4.8])
      fig.set_title('Dbscan clustering')
      subplot = fig.subplot(1,3,1); self.plot_2D_features(ax=(0,1),plt=subplot)
      subplot = fig.subplot(1,3,2); self.plot_2D_features(ax=(1,2),plt=subplot)
      subplot = fig.subplot(1,3,3); self.plot_2D_features(ax=(2,0),plt=subplot)
      if not embedded:
        plt.show()

  def wrap_3D_features(self, fig=None, embedded=False):
      if not fig:
        fig = plt.figure(figsize=[9.6,9.6])
      fig.gca().set_title('Dbscan clustering')
      fig.gca().axis("off")
      gsp = GridSpec(2,2)
      subplot1 = fig.add_subplot(gsp[0]);                  self.plot_2D_features(ax=(0,1),plt=subplot1)
      subplot2 = fig.add_subplot(gsp[1], sharey=subplot1); self.plot_2D_features(ax=(2,1),plt=subplot2)
      subplot3 = fig.add_subplot(gsp[2], sharex=subplot1); self.plot_2D_features(ax=(0,2),plt=subplot3)
      subplot3.invert_yaxis()
      inner = gsp[3].subgridspec(3, 3, width_ratios=[1,8,1],height_ratios=[1,6,3])
      subplot4 = fig.add_subplot(inner[1,1]);
      self.qq(subplot4)
      fig.text(s=self.legend,x=0.5,y=0.1,fontsize="small",fontfamily="monospace")
      if not embedded:
        if self.is_monoclinic: self.wrap_monoclinic_features()
        elif self.is_triclinic: self.wrap_triclinic_features()
        else: plt.show()

  PER_BIN = 100
  def wrap_monoclinic_features(self):
      UC_train = self.FV.UC_train
      assert len(UC_train) == len(self.Lab)
      BIN = len(UC_train)//self.PER_BIN

      fig = plt.figure(figsize=[9.6,4.8])
      gsp = GridSpec(1,2)
      subplot1 = fig.add_subplot(gsp[0])
      subplot2 = fig.add_subplot(gsp[1], sharey=subplot1);
      subplot1.set_title("Histogram of unit cell volume")
      subplot2.set_title("Histogram of monoclinic angle")
      subplot1.set_xlabel("Unit cell volume (nm$^3$)")
      subplot2.set_xlabel("Unit cell β angle (°)")

      allbetas = [UC_train[i].parameters()[4] for i in range(len(UC_train)) if self.pop.reverse_lookup[self.Lab[i]]>=0]
      minbeta,maxbeta = min(allbetas), max(allbetas)
      allvols = [UC_train[i].volume()/1000. for i in range(len(UC_train)) if self.pop.reverse_lookup[self.Lab[i]]>=0]
      minvol,maxvol= min(allvols), max(allvols)
      for jitem in range(len(self.pop.fit_components)):
        betas = [UC_train[i].parameters()[4] for i in range(len(UC_train)) if self.pop.reverse_lookup[self.Lab[i]]==jitem]
        subplot2.hist(betas, bins = BIN, range = (minbeta,maxbeta), histtype="stepfilled", color = self.appcolors[jitem], label="%d"%(jitem))
        vols = [UC_train[i].volume()/1000. for i in range(len(UC_train)) if self.pop.reverse_lookup[self.Lab[i]]==jitem]
        subplot1.hist(vols, bins = BIN, range = (minvol, maxvol), histtype="stepfilled", color = self.appcolors[jitem])
        print ("component %d, betas %d, vols %d"%(jitem, len(betas), len(vols)))
      subplot2.legend(loc='upper right')

      plt.show()

  def wrap_triclinic_features(self):
      UC_train = self.FV.UC_train
      assert len(UC_train) == len(self.Lab)
      BIN = len(UC_train)//self.PER_BIN

      fig = plt.figure(figsize=[19.2,4.8])
      gsp = GridSpec(1,4)
      subplot1 = fig.add_subplot(gsp[0])
      subplot1.set_title("Histogram of unit cell volume")
      subplot1.set_xlabel("Unit cell volume (nm$^3$)")

      allvols = [UC_train[i].volume()/1000. for i in range(len(UC_train)) if self.pop.reverse_lookup[self.Lab[i]]>=0]
      minvol,maxvol= min(allvols), max(allvols)
      for jitem in range(len(self.pop.fit_components)):
        vols = [UC_train[i].volume()/1000. for i in range(len(UC_train)) if self.pop.reverse_lookup[self.Lab[i]]==jitem]
        subplot1.hist(vols, bins = BIN, range = (minvol, maxvol), histtype="stepfilled", color = self.appcolors[jitem])
        print ("component %d, vols %d"%(jitem, len(vols)))

      for igsp, iparam, symbol in [(1, 3,"α"),(2, 4,"β"),(3, 5,"γ")]:
        subplot2 = fig.add_subplot(gsp[igsp], sharey=subplot1);
        if igsp==2: subplot2.set_title("Histogram of triclinic angle")
        subplot2.set_xlabel("Unit cell %s angle (°)"%symbol)

        allangles = [UC_train[i].parameters()[iparam] for i in range(len(UC_train)) if self.pop.reverse_lookup[self.Lab[i]]>=0]
        minangle,maxangle = min(allangles), max(allangles)
        for jitem in range(len(self.pop.fit_components)):
          angles = [UC_train[i].parameters()[iparam] for i in range(len(UC_train)) if self.pop.reverse_lookup[self.Lab[i]]==jitem]
          subplot2.hist(angles, bins = BIN, range = (minangle,maxangle), histtype="stepfilled", color = self.appcolors[jitem], label="%d"%(jitem))
        if igsp==3: subplot2.legend(loc='upper right')
      plt.show()

'''kanban:
ability to either refilter on covariance matrix or not
ability to enter the plot range for each feature or not
ability to output COV object for reuse in merging worker.  Labelit by feature label
ability to zoom in on just one component at a time
Practical unit cell clustering in serial protein crystallography.(Analyze crystal packing contacts in all isoforms)
'''
def run_detail(params):
    plots = dbscan_plot_manager(params)
    #plots.plot_raw_uc()
    plots.wrap_3D_features()

if __name__=="__main__":
  params, options = app_set_up()
  params.input.space_group="P212121"
  params.input.feature_vector="(a,b,c)"
  params.show_plot=True
  params.file_name="swissfel_psII/sample_13.tdata"
  params.cluster.dbscan.eps=0.2
  run_detail(params)
