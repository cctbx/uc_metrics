from __future__ import division, print_function
from six.moves import range
from cctbx.array_family import flex
from libtbx.development.timers import SlimProfiler as Profiler
from libtbx import group_args
from iotbx.phil import parse
"""
Initial implementation of clustering by Rodriguez 2014 algorithm
"""
class clustering_manager(group_args):
  def __init__(self, **kwargs):
    group_args.__init__(self, **kwargs)
    # require Dij, d_c
    P = Profiler("2. calculate rho density")
    print("finished Dij, now calculating rho_i, the density")
    from xfel.clustering import Rodriguez_Laio_clustering_2014
    # alternative clustering algorithms: see http://scikit-learn.org/stable/modules/clustering.html
    # also see https://cran.r-project.org/web/packages/dbscan/vignettes/hdbscan.html
    # see also https://en.wikipedia.org/wiki/Hausdorff_dimension

    R = Rodriguez_Laio_clustering_2014(distance_matrix = self.Dij, d_c = self.d_c)
    self.rho = rho = R.get_rho()
    ave_rho = flex.mean(rho.as_double())
    NN = self.Dij.focus()[0]
    print("The average rho_i is %5.2f, or %4.1f%%"%(ave_rho, 100*ave_rho/NN))
    i_max = flex.max_index(rho)

    P = Profiler("3.transition")
    print("the index with the highest density is %d"%(i_max))
    delta_i_max = flex.max(flex.double([self.Dij[i_max,j] for j in range(NN)]))
    print("delta_i_max",delta_i_max)
    rho_order = flex.sort_permutation(rho,reverse=True)
    rho_order_list = list(rho_order)

    P = Profiler("4. delta")
    self.delta = delta = R.get_delta(rho_order=rho_order, delta_i_max=delta_i_max)

    P = Profiler("5. find cluster maxima")
    #---- Now hunting for clusters ---Lot's of room for improvement (or simplification) here!!!
    cluster_id = flex.int(NN,-1) # default -1 means no cluster
    delta_order = flex.sort_permutation(delta,reverse=True)
    N_CLUST = 10 # maximum of 10 points to be considered as possible clusters
    #MAX_PERCENTILE_DELTA = 0.99 # cluster centers have to be in the top 10% percentile delta
    MAX_PERCENTILE_RHO = 0.99 # cluster centers have to be in the top 75% percentile rho
    n_cluster = 0
    #max_n_delta = min(N_CLUST, int(MAX_PERCENTILE_DELTA*NN))
    for ic in range(NN):
      # test the density, rho
      item_idx = delta_order[ic]
      if delta[item_idx]>100:  print("A: iteration", ic, "delta", delta[item_idx], delta[item_idx] < 0.25 * delta[delta_order[0]])
      if delta[item_idx] < 0.25 * delta[delta_order[0]]: # too low (another heuristic!)
        continue
      item_rho_order = rho_order_list.index(item_idx)
      if delta[item_idx]>100:  print("B: iteration", ic, item_rho_order,item_rho_order/NN,MAX_PERCENTILE_RHO)
      if item_rho_order/NN < MAX_PERCENTILE_RHO :
        cluster_id[item_idx] = n_cluster
        print(ic,item_idx,item_rho_order,cluster_id[item_idx])
        n_cluster += 1
    print("Found %d clusters"%n_cluster)
    for x in range(NN):
      if cluster_id[x]>=0:
        print("XC",x,cluster_id[x],rho[x],delta[x])
    self.cluster_id_maxima = cluster_id.deep_copy()

    P = Profiler("6. assign all points")
    R.cluster_assignment(rho_order,cluster_id)

    self.cluster_id_full = cluster_id.deep_copy()

    # assign the halos
    P = Profiler("7. assign halos")
    halo = flex.bool(NN,False)
    border = R.get_border( cluster_id = cluster_id )

    for ic in range(n_cluster): #loop thru all border regions; find highest density
      print("cluster",ic, "in border",border.count(True))
      this_border = (cluster_id == ic) & (border==True)
      print(len(this_border), this_border.count(True))
      if this_border.count(True)>0:
        highest_density = flex.max(rho.select(this_border))
        halo_selection = (rho < highest_density) & (this_border==True)
        if halo_selection.count(True)>0:
          cluster_id.set_selected(halo_selection,-1)
        core_selection = (cluster_id == ic) & ~halo_selection
        highest_density = flex.max(rho.select(core_selection))
        too_sparse = core_selection & (rho.as_double() < highest_density/10.) # another heuristic
        if too_sparse.count(True)>0:
          cluster_id.set_selected(too_sparse,-1)
    self.cluster_id_final = cluster_id.deep_copy()
    print("%d in the excluded halo"%((cluster_id==-1).count(True)))

master_phil = """
file_name = lysozyme/lysozyme1341.tdata
  .type = path
  .multiple = False
  .help = lysozyme/lysozyme1341.tdata or lysozyme/lysozyme13004.tdata or lysozyme/lysozyme137347.tdata
input {
  space_group = None
    .type = str
    .help = space group symbol, mandatory if using tdata input to instantiate feature vectors directly
  feature_vector = None
    .type = str
    .help = unit cell parameters to be taken directly as features for the feature vector, i.e. "(a,b,c,beta)"
  iterable = None
    .type = str
    .multiple = True
    .help = The tdata in memory as a list of strings. Used in the ucinline worker to avoid ever writing to file.
    .help = Normally not used in the main application, uc_metrics.dbscan, as the tdata come from a file.
}
write_covariance = None
  .type = str
  .help = assuming components identified and modeled as Gaussian ellipsoids, output the models
  .help = later read the models in again so as to apply them as filter to merging data
metric = *NCDist NCDist2017 CS6Dist_in_G6 L2norm
  .type = choice
  .multiple = False
  .help = Three possible Andrews-Bernstein metrics, currently implemented, plus Euclidean norm
omp_num_threads = 64
  .type = int(value_min=1)
  .help = for OpenMP parallel execution
show_plot = False
  .type = bool
plot {
  ellipsoids = True
    .type = bool
    .help = for clarity, either plot the multivariate contour levels, or not
  outliers = True
    .type = bool
    .help = for clarity, either plot the cells not assigned to clusters, or not
  legend = None
    .type = str
    .help = Name of the dataset (if blank, use the filename)
  limits {
    # only implemented for 2D plots (i.e., lower symmetry would have to be implemented on a,b,c, not x,y)
    x = None
      .type = floats(size=2)
    y = None
      .type = floats(size=2)
  }
  precision = 2
    .type = int
    .help = number of decimal points in Angstroms for unit cell printout
}
filter {
  iqr_ratio = None
    .type = float
    .help = If not None, then do a preliminary round of outlier rejection.
}
cluster {
  algorithm = *dbscan GaussianMixture
    .type = choice
    .multiple = False
  dbscan {
    eps = 0.02
      .type = float
    min_samples = 5
      .type = int
  }
  gaussian_mixture {
    n_components = 2
      .type = int
    means_init = None
      .type = floats(size_min=1, size_max=6)
      .multiple = True
  }

}
"""
phil_scope = parse(master_phil)

def app_set_up():

    help_message = '''Basic clustering example'''

    # The script usage
    import libtbx.load_env
    usage = "usage: %s [options] [param.phil] " % libtbx.env.dispatcher_name
    parser = None
    from dials.util.options import ArgumentParser
    # Create the parser
    parser = ArgumentParser( usage=usage, phil=phil_scope, epilog=help_message)

    # Parse the command line. quick_parse is required for MPI compatibility
    params, options = parser.parse_args(show_diff_phil=True,quick_parse=True)

    # Log the modified phil parameters
    #diff_phil_str = parser.diff_phil.as_str()
    #if diff_phil_str is not "":
    #    print("The following parameters have been modified:\n%s"%diff_phil_str)

    return params, options

from xfel.clustering.singleframe import SingleFrame
class CellOnlyFrame(SingleFrame):
  """read in the raw data as a,b,c,alpha,beta,gamma,centring as string tokens in Angstroms, degrees, centering symbol
     convert to Niggli cell
     convert to G6 vector
  """
  def __init__(self, crystal_symmetry, path=None, name=None, lattice_id=None):
    Z=Profiler("Z1")
    self.crystal_symmetry = crystal_symmetry
    Z=Profiler("Z2")
    self.niggli_cell = self.crystal_symmetry.niggli_cell()
    Z=Profiler("Z5")
    self.uc = self.niggli_cell.unit_cell().parameters()

def cells_from_tdata(params):
    from cctbx import crystal
    cells = []
    G6_double = flex.double()

    for line in open(params.file_name, "r"):
      P = Profiler("0A1. Read data")
      tokens = line.strip().split()
      P = Profiler("0A2. Read data")
      unit_cell = tuple(float(x) for x in tokens[0:6])
      P = Profiler("0A3. Read data")
      space_group_symbol = tokens[6]
      P = Profiler("0A4. Read data")
      crystal_symmetry = crystal.symmetry(unit_cell = unit_cell, space_group_symbol = space_group_symbol)
      del P
      cells.append(CellOnlyFrame(crystal_symmetry,path=None))
      P = Profiler("0A5. make G6")
      for j in cells[-1].make_g6(cells[-1].uc): G6_double.append(j)

    print(("There are %d cells"%(len(cells))))
    return cells, G6_double

def get_metric_function(params):
    from cctbx.uctbx.determine_unit_cell import ( NCDist_flatten,NCDist2017_flatten,CS6Dist_in_G6_flatten,
      Euclidean_L2norm_flatten )


    return dict(NCDist = NCDist_flatten,
                NCDist2017 = NCDist2017_flatten,
                CS6Dist_in_G6 = CS6Dist_in_G6_flatten,
                L2norm = Euclidean_L2norm_flatten,
                )[params.metric]

def run_detail(save_plot):
    params, options = app_set_up()

    cells, G6_double = cells_from_tdata(params)

    coord_x = flex.double([c.uc[0] for c in cells]); coord_y = flex.double([c.uc[1] for c in cells])
    if params.show_plot or save_plot:
      import matplotlib
      if not params.show_plot:
        # http://matplotlib.org/faq/howto_faq.html#generate-images-without-having-a-window-appear
        matplotlib.use('Agg') # use a non-interactive backend
      from matplotlib import pyplot as plt
      plt.plot([c.uc[0] for c in cells],[c.uc[1] for c in cells],"k.", markersize=3.)
      plt.axes().set_aspect("equal")
      if save_plot:
        plt.savefig(plot_name,
                    size_inches=(10,10),
                    dpi=300,
                    bbox_inches='tight')
      if params.show_plot:
        plt.show()

    print("Now constructing a Dij matrix.")
    P = Profiler("1. compute Dij matrix")
    NN = len(cells)

    import omptbx
    omptbx.omp_set_num_threads(params.omp_num_threads)
    metric_function = get_metric_function(params)

    Dij = metric_function(G6_double) # loop is flattened

    del P

    d_c = 10 # the distance cutoff, such that average item neighbors 1-2% of all items
    CM = clustering_manager(Dij=Dij, d_c=d_c)

    # Summarize the results here
    n_cluster = 1+flex.max(CM.cluster_id_final)
    print(len(cells), "have been analyzed")
    print(("# ------------   %d CLUSTERS  ----------------"%(n_cluster)))
    for i in range(n_cluster):
      item = flex.first_index(CM.cluster_id_maxima, i)
      print("Cluster %d.  Central unit cell: item %d"%(i,item))
      cells[item].crystal_symmetry.show_summary()
      these_idx = (CM.cluster_id_final==i).iselection(True)
      these_cells = [cells[a] for a in these_idx]
      stddev = []
      for prm in range(6):
        all_p = flex.mean_and_variance(flex.double([t.crystal_symmetry.unit_cell().parameters()[prm] for t in these_cells]))
        stddev.append( all_p.unweighted_sample_standard_deviation() )
      print( "Stddev cell(%7.4f,%8.4f,%8.4f,%8.4f,%8.4f,%8.4f)"%tuple(stddev) )
      # start here, problems
      #   1) clusters should not be permitted to have <3 members
      #   2) apparent memory overflow for the 137000 case
      #   3) still cannot detect the third lattice for any subset of the 137000
      #   4) L2 metric does not give realistic cluster centroid; need 3D Gaussian fit centroid instead
      #   5) above 45000 unit cells, there is a new rate limiting step: "6. assign all points".  But for that size
      #        the Dij matrix is the second most time consuming step and also needs to be accelerated.
      #   6) above 67000 we are memory limited, 35 GB, segfault core dump
      #   7) for the L2 metric, the decision plot is plotted with a weird x-axis.
      print("Cluster has %d items, or %d after trimming borders (%.0f%% of total %d)"%(
        (CM.cluster_id_full==i).count(True),
        (CM.cluster_id_final==i).count(True),
        100.*(CM.cluster_id_final==i).count(True)/NN,
        NN
        ))
      print()

    appcolors = ['b', 'r', '#ff7f0e', '#2ca02c',
              '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
              '#bcbd22', '#17becf']
    if params.show_plot:
      #Decision graph
      from matplotlib import pyplot as plt

      plt.plot(CM.rho,CM.delta,"r.", markersize=3.)
      for x in range(NN):
        if CM.cluster_id_maxima[x]>=0:
          plt.plot([CM.rho[x]],[CM.delta[x]],"ro")
      plt.show()

      #No-halo plot
      from matplotlib import pyplot as plt
      colors = [appcolors[i%10] for i in CM.cluster_id_full]

      plt.scatter(coord_x,coord_y,marker='o',color=colors,
          linewidths=0.4, edgecolor='k')
      for i in range(n_cluster):
        item = flex.first_index(CM.cluster_id_maxima, i)
        plt.plot([cells[item].uc[0]],[cells[item].uc[1]],'y.')
      plt.axes().set_aspect("equal")
      plt.show()

      #Final plot
      halo = (CM.cluster_id_final==-1)
      core = ~halo
      plt.plot(coord_x.select(halo),coord_y.select(halo),"k.")
      colors = [appcolors[i%10] for i in CM.cluster_id_final.select(core)]
      plt.scatter(coord_x.select(core),coord_y.select(core),marker="o",
          color=colors,linewidths=0.4, edgecolor='k')
      for i in range(n_cluster):
        item = flex.first_index(CM.cluster_id_maxima, i)
        plt.plot([cells[item].uc[0]],[cells[item].uc[1]],'y.')
      plt.axes().set_aspect("equal")
      plt.show()

if __name__=="__main__":

  run_detail(save_plot=False)
""" Benchmark, Lysozyme lattices, Linux AMD Athlon, single process
          Lattices              1341        13004           20000      137347
          # cores                 64           64              64    intractable

0A4. crystal.symmetry: wall     0.31s;       3.01s;          4.60s;
      Z2. niggli cell: wall     0.30s;       2.87s;          4.47s;
1. compute Dij matrix: wall     2.68s;     207.14s;        489.95s;
         RL quick sum: wall     0.03s;       2.90s;         10.10s;
TOTAL                : wall     3.42s;     220.60s;        510.62s;
"""
