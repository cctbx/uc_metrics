# -*- mode: python; coding: utf-8; indent-tabs-mode: nil; python-indent: 2 -*-
from __future__ import division, print_function
import math
import numpy as np

class get_population_permutation:
  """the component labels should be reorganized so that the largest
  component is "0", second largest is "1", etc.  This function
  returns the sort order"""

  def __init__(self, labels):
    unique_labels = set(labels)
    self.n_clusters_ = len(unique_labels) - (1 if -1 in labels else 0)
    self.n_noise_ = list(labels).count(-1)

    self.n_noise_ = list(labels).count(-1)

    populations = {}

    for k in unique_labels:
      if k == -1:
        continue
      class_member_mask = (labels == k)
      xy = labels[class_member_mask]
      populations[k] = len(xy)

    from scitbx.array_family import flex # would like a pure-Python alternative
                                         # but it would have to properly track the sort permutation
    values = flex.size_t(list(populations.values()))
    components = flex.sort_permutation(values, reverse=True)
    reverse_lookup = [list(components).index(x) for x in range(len(components))]+[-1] # preserve the -1
    print (populations, "noise:", self.n_noise_, "order", list(components))

    MIN_CLUST = 10 # under no circumstance permit cluster with fewer members
    MIN_FRAC = 0.001 # smallest percentage population allowable
    main_components = [i for i in components if values[i] > MIN_CLUST and values[i] > MIN_FRAC * len(labels)]
    #print( ", ".join(["%d:%d"%(i,populations[i]) for i in main_components]) )
    """Explanation:  label is the tag for each sample given by the clustering engine; integer >= 0 except outlier=-1
    """
    self.reverse_lookup = reverse_lookup
    self.labels = labels
    self.populations = populations
    self.main_components = main_components

  def basic_covariance(self, feature_vectors):
    """report out covariance of the main components"""
    features = feature_vectors.features_
    train = feature_vectors.fv_
    from sklearn.covariance import EmpiricalCovariance
    self.fit_components = []
    for jitem, idx_com in enumerate(self.main_components): # loop thru starting from largest component
      class_member_mask = (self.labels == idx_com)
      X = train[class_member_mask, :]
      # compare estimators learnt from the full data set with true parameters
      emp_cov = EmpiricalCovariance(assume_centered=False, store_precision=True).fit(X)
      self.fit_components.append(emp_cov)

      #print("Component %d: with %d items (%5.2f%%)"%(jitem,len(X),100.*len(X)/len(self.labels)))
      for idx_report in range(len(features)):
        feature = features[idx_report]
        diag_elem = math.sqrt(emp_cov.covariance_[idx_report,idx_report])
        #print( "%5s = %7.3f ± %7.3f"%(feature, emp_cov.location_[idx_report], diag_elem))
      #print()

  def basic_covariance_compact_report(self, feature_vectors, params=None):
    """report out covariance of the main components"""
    features = feature_vectors.features_
    from io import StringIO
    S = StringIO()
    print ("%s, %s"%(feature_vectors.sample_name,feature_vectors.output_info), file=S)
    print ("Component        Abundance", end=" ",file=S)
    tokens=[]
    for idx_report in range(len(features)):
        tokens.append("%11s"%(features[idx_report]))
    print("  ".join(tokens),file=S)
    for jitem, component in enumerate(self.fit_components): # loop thru starting from largest component
      len_x = self.populations[self.main_components[jitem]]
      print("%d: %6d /%6d (%5.2f%%)"%(jitem,len_x,len(self.labels),100.*len_x/len(self.labels)),end=" ",file=S)
      tokens=[]
      for idx_report in range(len(features)):
        feature = features[idx_report]
        diag_elem = math.sqrt(component.covariance_[idx_report,idx_report])
        unit = ""
        if features[idx_report] in ["a","b","c"]:
          unit = "Å"
        if params is not None:
          pformat = "%%7.%1df±%%.%1df%%s"%(params.plot.precision, params.plot.precision)
          tokens.append(pformat%(component.location_[idx_report], diag_elem, unit))
        else:
          tokens.append("%7.2f±%.2f%s"%(component.location_[idx_report], diag_elem, unit))
      print(" ".join(tokens),file=S)
    return S

  def cov_component(self,jitem,train,idx_com,features):  # draft code
    """covariance analysis of component number idx_com"""

    from sklearn.covariance import EmpiricalCovariance, MinCovDet
    class_member_mask = (self.labels == idx_com)
    X = train[class_member_mask, :]
    # compare estimators learnt from the full data set with true parameters
    emp_cov = EmpiricalCovariance(assume_centered=False, store_precision=True).fit(X)

    print("Component %d: with %d items (%5.2f%%)"%(jitem,len(X),100.*len(X)/len(self.labels)))
    for idx_report in range(len(features)):
      feature = features[idx_report]
      diag_elem = math.sqrt(emp_cov.covariance_[idx_report,idx_report])
      print( "%s = %7.3f ± %7.3f"%(feature, emp_cov.location_[idx_report], diag_elem))
    print()

    import numpy as np
    import matplotlib.pyplot as plt

    # fit a Minimum Covariance Determinant (MCD) robust estimator to data
    robust_cov = MinCovDet(assume_centered=False, store_precision=True).fit(X)

# #############################################################################
    # Display results
    n_samples = len(X)
    n_features = X.shape[1]

    fig = plt.figure()
    #plt.subplots_adjust(hspace=-.1, wspace=.4, top=.95, bottom=.05)

    # Show data set
    subfig1 = plt.subplot(3, 1, 1)
    inlier_plot = subfig1.scatter(X[:, 0], X[:, 1],
                                  color='black', label='inliers')
    #from IPython import embed; embed()
    subfig1.scatter( [emp_cov.location_[0]], [emp_cov.location_[1]], color="yellow", label="mean")

    #subfig1.set_xlim(subfig1.get_xlim()[0], 11.)
    subfig1.set_title("Mahalanobis distances of a contaminated data set:")

    # Show contours of the distance functions
    deltax = plt.xlim()[1] - plt.xlim()[0]
    deltay = plt.ylim()[1] - plt.ylim()[0]
    xx, yy = np.meshgrid(np.linspace(plt.xlim()[0]-(deltax/2.), plt.xlim()[1]+(deltax/2.), 100),
                         np.linspace(plt.ylim()[0]-(deltay/2.), plt.ylim()[1]+(deltay/2.), 100))
    zz = np.c_[xx.ravel(), yy.ravel()]

    mahal_emp_cov = emp_cov.mahalanobis(zz)
    mahal_emp_cov = mahal_emp_cov.reshape(xx.shape)
    emp_cov_contour = subfig1.contour(xx, yy, np.sqrt(mahal_emp_cov),
                                  levels=[1.,2.,3.,4.],
                                  cmap=plt.cm.PuBu_r,
                                  linestyles='dashed')

    mahal_robust_cov = robust_cov.mahalanobis(zz)
    mahal_robust_cov = mahal_robust_cov.reshape(xx.shape)
    robust_contour = subfig1.contour(xx, yy, np.sqrt(mahal_robust_cov),
                                 levels=[1.,2.,3.,4.],
                                 cmap=plt.cm.YlOrBr_r, linestyles='dotted')

    subfig1.legend([emp_cov_contour.collections[1], robust_contour.collections[1],
                inlier_plot],
               ['MLE dist', 'robust dist', 'inliers'],
               loc="upper right", borderaxespad=0)
    #plt.xticks(())
    #plt.yticks(())

    # Plot the scores for each point
    emp_mahal = emp_cov.mahalanobis(X - np.mean(X, 0)) ** (0.33)
    subfig2 = plt.subplot(2, 2, 3)
    subfig2.boxplot([emp_mahal[:], ], widths=.25)
    subfig2.plot(np.full(n_samples, 1.26),emp_mahal[:], '+k', markeredgewidth=1)
    subfig2.axes.set_xticklabels(('inliers'), size=15)
    subfig2.set_ylabel(r"$\sqrt[3]{\rm{(Mahal. dist.)}}$", size=16)
    subfig2.set_title("1. from non-robust estimates\n(Maximum Likelihood)")
    plt.yticks(())

    robust_mahal = robust_cov.mahalanobis(X - robust_cov.location_) ** (0.33)
    subfig3 = plt.subplot(2, 2, 4)
    subfig3.boxplot([robust_mahal[:],],
                widths=.25)
    subfig3.plot(np.full(n_samples, 1.26),
             robust_mahal[:], '+k', markeredgewidth=1)

    subfig3.axes.set_xticklabels(('inliers'), size=15)
    subfig3.set_ylabel(r"$\sqrt[3]{\rm{(Mahal. dist.)}}$", size=16)
    subfig3.set_title("2. from robust estimates\n(Minimum Covariance Determinant)")
    plt.yticks(())

    plt.show()

