# LIBTBX_SET_DISPATCHER_NAME uc_metrics.list_covariance
from __future__ import absolute_import, division, print_function
"""command to decode the pickle files written by the population_permutation manager"""
from uc_metrics.clustering.util import get_population_permutation # implicit import
import sys, pickle

path = sys.argv[1]
class Empty: pass

with open(path,'rb') as F:
  data = pickle.load(F)
  E=Empty()
  E.features_ = data["features"]
  E.sample_name = data["sample"]
  E.output_info = data["info"]
  pop=data["populations"]
  print("There are %d cells"%(len(pop.labels)))
  print(pop.populations, "noise:", pop.n_noise_, "order", pop.main_components)

  legend = pop.basic_covariance_compact_report(feature_vectors=E).getvalue()
  print(legend)
sample_output = """
% uc_metrics.list_covariance covariance_sample_12.pickle
There are 6907 cells
{0: 3438, 1: 2570, 2: 12, 3: 14, 4: 21, 5: 5, 6: 7, 7: 6, 8: 5, 9: 5, 10: 8, 11: 4, 12: 5} noise: 807 order [0, 1, 4, 3, 2, 10, 6, 7, 5, 8, 9, 12, 11]
sample_12, eps=0.300
Component        Abundance           a            b            c
0:   3438 /  6907 (49.78%)  116.73±0.16  222.74±0.43  328.25±0.85
1:   2570 /  6907 (37.21%)  116.77±0.20  222.16±0.65  308.63±0.91
2:     21 /  6907 ( 0.30%)  116.70±0.13  222.32±0.30  312.47±0.21
3:     14 /  6907 ( 0.20%)  116.83±0.12  221.66±0.27  311.10±0.12
4:     12 /  6907 ( 0.17%)  116.67±0.17  222.26±0.13  314.17±0.21
"""
