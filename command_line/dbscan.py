# LIBTBX_SET_DISPATCHER_NAME uc_metrics.dbscan
from __future__ import absolute_import, division, print_function
from uc_metrics.clustering.step1 import app_set_up
from uc_metrics.clustering.step_dbscan3d import dbscan_plot_manager
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec

def wrap_2D_features(plots):
  fig = plt.figure(figsize=[9.6,9.6])
  plt.title('Dbscan clustering')
  plt.axis("off")
  gsp = GridSpec(1,1)
  subplot1 = fig.add_subplot(gsp[0]);                  plots.plot_2D_features(ax=(0,1),plt=subplot1)
  fig.text(s=plots.legend,x=0.5,y=0.1,fontsize="small",fontfamily="monospace")
  plt.show()

def run_detail(params):
    plots = dbscan_plot_manager(params)
    params.show_plot=True
    #plots.plot_raw_uc()
    number_of_features = len(params.input.feature_vector.split(","))
    # simplistic method for finding the plot type!
    if number_of_features <= 2:
      wrap_2D_features(plots)
    else:
      plots.wrap_3D_features()

if __name__=="__main__":
  params, options = app_set_up()
  run_detail(params)

