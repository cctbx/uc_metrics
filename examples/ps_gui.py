from __future__ import division, print_function
from matplotlib import pyplot as plt
import os

"""
Initial implementation of clustering by sklearn's DBSCAN
"""

from uc_metrics.examples.step1 import app_set_up
from uc_metrics.examples.step_dbscan import feature_vectors

def run_detail(save_plot):
    params, options = app_set_up()

    params.input.space_group = "P212121"
    params.input.feature_vector = "(a,b,c,alpha,beta,gamma)"
    FV = feature_vectors.from_tdata(params)
    X_train = FV.fv_
    one_tdata = []
    for item in X_train:
      one_tdata.append(dict(a=item[0],b=item[1],c=item[2],alpha=item[3],beta=item[4],gamma=item[5]))
    data = [one_tdata]
    if params.show_plot or save_plot:
      import matplotlib
      if not params.show_plot:
        # http://matplotlib.org/faq/howto_faq.html#generate-images-without-having-a-window-appear
        matplotlib.use('Agg') # use a non-interactive backend
      plt.plot(coord_x,coord_y,"k.", markersize=3.)
      plt.axes().set_aspect("equal")
      if save_plot:
        plt.savefig(plot_name,
                    size_inches=(10,10),
                    dpi=300,
                    bbox_inches='tight')
      if params.show_plot:
        plt.show()
    from xfel.ui.components.xfel_gui_plotter import PopUpCharts
    c = PopUpCharts()
    legend = os.path.splitext(os.path.basename(params.file_name))[0]
    c.plot_uc_histogram(data,legend_list=[legend])
    plt.show()

    exit("popup")


if __name__=="__main__":

  run_detail(save_plot=False)

