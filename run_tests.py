from __future__ import division, print_function
from libtbx import test_utils
import libtbx.load_env

tst_list = (
  "$D/tests/ncdist_stability.py",
  "$D/tests/tst_s6dist.py",
  )

def run_standalones():
  build_dir = libtbx.env.under_build("uc_metrics")
  dist_dir = libtbx.env.dist_path("uc_metrics")

  test_utils.run_tests(build_dir, dist_dir, tst_list)

if (__name__ == "__main__"):
  run_standalones()
