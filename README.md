# cluster_regression
Utils for testing the Andrews-Bernstein metric

## Basic usage

First we create a "tdata" file containing unit cells and space groups. Currently it's fastest to extract
cell basis vectors from processed experiments as follows:

```
dwpaley@drp-srcf-eb002:/cds/data/drpsrcf/mfx/mfxly6420/scratch/common/results/r0062/003_rg021
$ egrep -A3 'real_space_[abc]' out/*_integrated.expt \
  |tr -d ',' \
  |sed '/real_space/d' \
  |sed '/--/d' \
  |awk '{print $2}' \
  > cells.txt
```

A utility script is provided to convert the basis vectors to unit cells:

```
dwpaley@drp-srcf-eb002:/cds/data/drpsrcf/mfx/mfxly6420/scratch/common/results/r0062/003_rg021
$ python `libtbx.find_in_repositories uc_metrics`/util/make_tdata.py \
  cells.txt P1211 \
  > cells.tdata
```

Perform the DBSCAN clustering. Note that `iqr_ratio` applies a rough prefilter to reject extreme
outliers.
```
dwpaley@drp-srcf-eb002:/cds/data/drpsrcf/mfx/mfxly6420/scratch/common/results/r0062/003_rg021
$ uc_metrics.dbscan \
  file_name=cells.tdata \
  input.space_group=P1211 \
  input.feature_vector=a,b,c \
  eps=0.2 \
  write_covariance=cells.pickle \
  iqr_ratio=1.5
```

<img src='images/clustered1.png' width='400'>

The clustering plots correctly label the two clusters, and the covariance matrices (ellipsoidal
contours) are a reasonable fit to the real distributions. The contours are plotted at Mahalanobis
distances of 1, 2, 3, and 4 from their respective centers. If the clustering did not succeed, the
first troubleshooting step would be testing additional values of `cluster.dbscan.eps`, which is a
maximum distance for two unit cells to be considered similar.

Place the resulting covariance file in a memorable location:
```
dwpaley@drp-srcf-eb002:/cds/data/drpsrcf/mfx/mfxly6420/scratch/common/results/r0062/003_rg021
$ cp covariance_cells.pickle ../../../../cov/run62.pickle
```

Finally, the following lines are added to the scaling phil file:
```
filter.algorithm=unit_cell
filter.unit_cell.algorithm=cluster
filter.unit_cell.cluster.covariance.file=/path/to/cov_file.pickle
filter.unit_cell.cluster.covariance.component=0
filter.unit_cell.cluster.covariance.mahalanobis=4
```


## Difficult example: clustering with Gaussian mixture model

A protein sample had two orthorhombic isoforms with barely resolved unit cells:

<img src='images/uc_hist.png' width='400'>

DBSCAN clustering was difficult although a grid search of `eps` parameters was able to identify the two clusters:

```
dwpaley@drp-srcf-eb002:/cds/data/drpsrcf/mfx/mfxly6420/scratch/common/results/r0080/006_rg025/combined
$ for eps in {020..010}; do 
    uc_metrics.dbscan \
      file_name=cells.tdata \
      input.space_group=P212121 \
      input.feature_vector=a,b,c \
      eps=0.$eps \
      write_covariance=cells.pickle
    echo "that was $eps"
  done
```

<img src='images/hard_dbscan.png' width=400>

Finally, a Gaussian mixture model is used to separate the clusters and determine realistic covariance matrices.
It was necessary to initialize the gmm with cluster centers that were read off the previously performed DBSCAN
attempt:
```
dwpaley@drp-srcf-eb002:/cds/data/drpsrcf/mfx/mfxly6420/scratch/common/results/r0080/006_rg025/combined
$ uc_metrics.dbscan \
  file_name=cells.tdata \
  input.space_group=P212121 \
  input.feature_vector=a,b,c \
  write_covariance=cells.pickle \
  cluster.algorithm=GaussianMixture \
  means_init=94.28,98.9,184.9 \
  means_init=94.31,99.1,185.0 \
  iqr_ratio=1.5
```
<img src='images/hard_gauss.png' width=400>
