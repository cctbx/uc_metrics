import sys
import numpy as np

def angle(v1, v2, degrees=True):
  v1_unit = v1/np.linalg.norm(v1)
  v2_unit = v2/np.linalg.norm(v2)
  dot = np.dot(v1_unit,v2_unit)
  rad = np.arccos(dot)
  if degrees: return rad*360/2/np.pi
  else: return rad

def print_cell_and_s6(basis, sg_string):
  assert basis.shape == (3,3)
  a, b, c = basis
  d = -a-b-c
  lengths = [np.linalg.norm(ax) for ax in basis]
  al = angle(b,c)
  be = angle(a,c)
  ga = angle(a,b)
  s6dots = [np.dot(x,y) for x,y in [
      (b,c), (a,c), (a,b), (a,d), (b,d), (c,d)
  ]]
  fstring = '{:.4f} ' * 6 + '{} ' + '{:.4f} ' * 6
  print(fstring.format(*lengths, al, be, ga, sg_string, *s6dots))

def run():
  sg_string = sys.argv[2]
  with open(sys.argv[1]) as f:
    inp = np.array([float(l) for l in f])
    direct_bases = inp.reshape(-1,3,3)
    for basis in direct_bases:
      print_cell_and_s6(basis, sg_string)

if __name__=='__main__':
  run()
