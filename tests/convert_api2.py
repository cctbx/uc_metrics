from __future__ import division
import sys, pickle
from libtbx.development.timers import Profiler
#file = sys.argv[1]
#newfile = sys.arg[2]
"""Script takes an old covariance file written with sklearn version prior to 0.24.
   writes out a pickle file compatible with 0.24 or higher.
   This conversion script must be run with version 0.22 or 0.23 where the API compatibility overlaps"""
file = "/global/cscratch1/sd/nksauter/adse13_187/covariance_cytochrome.pickle"
newfile = "/global/cscratch1/sd/nksauter/adse13_187/covariance_cytochrome_0.24.pickle"
dictfile = "/global/cscratch1/sd/nksauter/adse13_187/covariance_cytochrome_form.pickle"

def convert_from_sklearn_to_sklearn_form(UUdict):
  newUUdict = {}
  for key in UUdict:
    if key != "populations":
      newUUdict[key] = UUdict[key]
    else:
      PP = UUdict["populations"]
      from uc_metrics.clustering.util import get_population_permutation
      newUUdict["populations"] = get_population_permutation(labels=PP.__dict__["labels"])
      newUUdict["populations"].fit_components = []
      for ifc in range(len(PP.fit_components)):
        FF = PP.fit_components[ifc]
        from sklearn.covariance import EmpiricalCovariance
        EC = EmpiricalCovariance(assume_centered=FF.assume_centered, store_precision=FF.store_precision)
        EC.covariance_=FF.covariance_
        EC.location_=FF.location_
        EC.precision_=FF.precision_
        newUUdict["populations"].fit_components.append(EC)
  return newUUdict

def convert_from_sklearn_to_plain_python_form(UUdict):
  newUUdict = {}
  for key in UUdict:
    if key != "populations":
      newUUdict[key] = UUdict[key]
    else:
      PP = UUdict["populations"]
      from uc_metrics.clustering.util import get_population_permutation
      newUUdict["populations"] = get_population_permutation(labels=PP.__dict__["labels"])
      newUUdict["populations"].fit_components = []
      for ifc in range(len(PP.fit_components)):
        FF = PP.fit_components[ifc]
        EC = dict(
          assume_centered=FF.assume_centered,
          store_precision=FF.store_precision,
          covariance_=FF.covariance_,
          location_=FF.location_,
          precision_=FF.precision_)
        newUUdict["populations"].fit_components.append(EC)
  return newUUdict

def convert_from_plain_python_to_sklearn_form(UUdict):
  newUUdict = {}
  for key in UUdict:
    if key != "populations":
      newUUdict[key] = UUdict[key]
    else:
      PP = UUdict["populations"]
      from uc_metrics.clustering.util import get_population_permutation
      newUUdict["populations"] = get_population_permutation(labels=PP.__dict__["labels"])
      newUUdict["populations"].fit_components = []
      for ifc in range(len(PP.fit_components)):
        FF = PP.fit_components[ifc]
        from sklearn.covariance import EmpiricalCovariance
        EC = EmpiricalCovariance(assume_centered=FF["assume_centered"], store_precision=FF["store_precision"])
        EC.covariance_=FF["covariance_"]
        EC.location_=FF["location_"]
        EC.precision_=FF["precision_"]
        newUUdict["populations"].fit_components.append(EC)
  return newUUdict

def confirm_identity(old,new):
  for key in old:
    print (key)
    if key != "populations":
      assert old[key] == new[key]
    else:
      PP = old["populations"]
      NN = new["populations"]
      print (type(PP))
      print ("OLD",PP.__dict__)
      print (type(NN))
      print ("NEW",NN.__dict__)
      for ekey in PP.__dict__:
        print (ekey)
        if ekey in ["n_clusters_","n_noise_","reverse_lookup","populations","main_components"]:
          assert PP.__dict__[ekey] == NN.__dict__[ekey]
        elif ekey in ["labels"]:
          assert list(PP.__dict__[ekey]) == list(NN.__dict__[ekey])
        else: # "fit_components":
          for ifc in range(len(PP.fit_components)):
            FF = PP.fit_components[ifc]
            GG = NN.fit_components[ifc]
            print (FF, GG)
            print(dir(FF)); print(dir(GG))
            for attrib in ["assume_centered","store_precision"]:
              print(attrib, getattr(FF,attrib), getattr(GG,attrib))
              assert getattr(FF,attrib) == getattr(GG,attrib)
            for attrib in ["covariance_","location_","precision_"]:
              print(attrib, getattr(FF,attrib), getattr(GG,attrib))
              assert getattr(FF,attrib).all() == getattr(GG,attrib).all()

if __name__=="__main__":
  P = Profiler("basic")
  OLD = pickle.load(open(newfile,"rb"))
  #print(OLD)
  #NEW = convert_from_sklearn_to_plain_python_form(OLD)
  #pickle.dump(NEW, open(dictfile,"wb"))
  NEW = convert_from_plain_python_to_sklearn_form(pickle.load(open(dictfile,"rb")))
  confirm_identity(OLD, NEW)
