from __future__ import division, print_function
from cctbx.uctbx.determine_unit_cell import CS6Dist,CS6Dist_in_G6
from libtbx.test_utils import approx_equal
from cctbx.crystal import symmetry
from xfel.clustering.singleframe import SingleFrame

def run_trivial():
  # Trivial example
  a = 0, 0, 0, -10, -10, -10 # Delaunay scalars
  c = 10, 10, 10, 0, 0, 0 #G6 version
  b = 0, 0, 0, -11, -11, -11 # Delaunay scalars
  d = 11, 11, 11, 0, 0, 0 #G6 version
  d1 = CS6Dist(a, b)
  d2 = CS6Dist(b, a)
  d3 = CS6Dist_in_G6(c, d)
  d4 = CS6Dist_in_G6(d, c)
  known = 1.73205080757 # centos 7, gcc 4.8.5
  assert approx_equal(d1, known)
  assert approx_equal(d2, known)
  assert approx_equal(d1, d2)
  assert approx_equal(d3, known)
  assert approx_equal(d4, known)
  assert approx_equal(d3, d4)
  print ("OK")

def run_cctbx_cell():
  sym1 = symmetry((20,20,30,90,90,90), "I4122")
  sym2 = symmetry((21,21,31,90,90,90), "I4122")

  cell1 = SingleFrame.make_g6(sym1.niggli_cell().unit_cell().parameters())
  cell2 = SingleFrame.make_g6(sym2.niggli_cell().unit_cell().parameters())

  assert approx_equal(CS6Dist_in_G6(cell1, cell2), 41.334761400057765) # centos 7, gcc 4.8.5

def run_all():
  run_cctbx_cell()
  run_trivial()

if __name__ == "__main__":
  run_all()
