<pre>
Instructions to test Andrews/Bernstein NCDist in the context of CCTBX. rev. 12/09/2019. Purpose:
1) Develop with confidence that nothing has broken (assert that past results are repeated)
2) Compare results and performance of G6, S6, and D7 metrics.
3) Measure OpenMP performance.

Pre-requisites.
1) Linux box with multiple cores (for OpenMP).  Use bash shell.
2) Make sure your conda environment file (~/.condarc) has the following:
channels:
  - cctbx
  - conda-forge
  - defaults
  - bioconda

report_errors: false
3) Create and change to a working directory ${WORK}

wget https://raw.githubusercontent.com/cctbx/cctbx_project/master/libtbx/auto_build/bootstrap.py
python bootstrap.py --builder=xfel --nproc=64 --use-conda --python3 --config_flags="--enable_openmp_if_possible=True"
# perform all steps: hot update base build
#(don't source the sources)
#(find your conda if you don't have it in your path, check ~/.conda/environments.txt)
source ${YOUR_CONDA}/etc/profile.d/conda.sh # or .csh
conda create -n gitlfs_env
conda activate gitlfs_env
conda install git git-lfs
cd modules/uc_metrics
git lfs install --local
git lfs pull
conda deactivate
cd ../../
source build/setpaths.sh
mkdir test; cd test
#rm -rf *
#export OMP_NUM_THREADS=8
libtbx.run_tests_parallel module=uc_metrics
</pre>
